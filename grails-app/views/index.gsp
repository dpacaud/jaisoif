<html>
<head>
    <title>Google Maps JavaScript API v3 Example: Map Geolocation</title>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <!--
    Include the maps javascript with sensor=true because this code is using a
    sensor (a GPS locator) to determine the user's location.
    See: https://developers.google.com/apis/maps/documentation/javascript/basics#SpecifyingSensor
    -->
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=true&libraries=places"></script>
    <script src="http://code.jquery.com/jquery-1.7.1.min.js"></script>
    <style type="text/css">
    html, body {
        height: 100%;
        margin: 0;
        padding: 0;
    }

    #map-canvas, #map_canvas {
        height: 100%;
    }

    @media print {
        html, body {
            height: auto;
        }

        #map_canvas {
            height: 650px;
        }
    }
    </style>
    <script>
        var map;
        var infowindow;
        var currentPos;

        function initialize() {
            var mapOptions = {
                zoom: 17,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            if(navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function(position) {
                    currentPos = new google.maps.LatLng(position.coords.latitude,
                            position.coords.longitude);
                    map = new google.maps.Map(document.getElementById('map-canvas'),mapOptions);

                    // catégorie bar : 4bf58dd8d48988d116941735

                    //https://developer.foursquare.com/docs/explore#req=venues/search%3Fll%3D48.89,2.35%26categoryId%3D4bf58dd8d48988d116941735%26client_id%3DP1EHAY5V2B4PLKR//QLRZLCHMXTRLQNS05VC2Z4X1AIXQFOVHN%26client_secret%3DUZTQP4L0YBND2YUV2KWJGD5FARANZVAKYJAARKFX0TKKFMHP%26v%3D20130323

                    // on ajoute le marker de la position courante
                    infowindow= new google.maps.InfoWindow({
                        map: map,
                        position: currentPos
                        //content: 'This is your location : ' + currentPos
                    });
                    var myMarker = new google.maps.Marker({
                        map: map,
                        position: currentPos,
                        draggable:true
                    });
                    map.setCenter(currentPos);

                    var request = {
                        location : currentPos,
                        radius : 500,
                        types : ['bar']
                    }
                    // Recherche places google
                    var service = new google.maps.places.PlacesService(map);
                    var googleBars, fourSquareBars;
                    service.nearbySearch(request, function(results,status){
                        if (status == google.maps.places.PlacesServiceStatus.OK) {
                            //console.log(currentPos);
                            //if we got google answer, we try 4square
                            googleBars = results;
                            //console.log(googleBars[0].geometry);
                            for (var i = 0; i < googleBars.length; i++) {
                                console.log(googleBars[i].name);
                                var place = googleBars[i];
                                createMarkerGoogle(googleBars[i]);
                            }
                            $.ajax({
                                url: "http://localhost:8080/jaisoif/bar/getBarsAroundMe4Square",
                                data:{lat:currentPos.kb,lng:currentPos.lb}
                            }).done(function(data) {
                                        fourSquareBars = data;
                                        console.log(fourSquareBars[1].location.lat);
                                        for (var i = 0; i < fourSquareBars.length; i++) {
                                            createMarker4Square(fourSquareBars[i]);
                                        }

                             });
                        }
                    });

                }, function() {
                    handleNoGeolocation(true);
                });
            } else {
                // Browser doesn't support Geolocation
                handleNoGeolocation(false);
            }
        }

        function handleNoGeolocation(errorFlag) {
            if (errorFlag) {
                var content = 'Error: The Geolocation service failed.';
            } else {
                var content = 'Error: Your browser doesn\'t support geolocation.';
            }

            var options = {
                map: map,
                position: new google.maps.LatLng(60, 105),
                content: content
            };

            var infowindow = new google.maps.InfoWindow(options);
            map.setCenter(options.position);
        }

        function createMarkerGoogle(place) {
            var placeLoc = place.geometry.location;
            var marker = new google.maps.Marker({
                map: map,
                position: place.geometry.location
            });

            google.maps.event.addListener(marker, 'click', function() {
                infowindow.setContent(place.name);
                infowindow.open(map, this);
            });
        }

        function createMarker4Square(obj){
            console.log("adding " + obj.name);
            console.log(obj.location.lat);
            var latLng = new google.maps.LatLng(obj.location.lat,obj.location.lng);
            console.log(latLng);
            var marker = new google.maps.Marker ({
                map: map,
                position: latLng
            }) ;
            google.maps.event.addListener(marker, 'click', function() {
                infowindow.setContent("4square " + obj.name);
                infowindow.open(map, this);
            });
        }

        google.maps.event.addDomListener(window, 'load', initialize);
    </script>
</head>
<body>
<div id="map-canvas"></div>
</body>
</html>

