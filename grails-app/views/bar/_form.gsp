<%@ page import="jaisoif.Bar" %>



<div class="fieldcontain ${hasErrors(bean: barInstance, field: 'lat', 'error')} required">
	<label for="lat">
		<g:message code="bar.lat.label" default="Lat" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="lat" value="${fieldValue(bean: barInstance, field: 'lat')}" required=""/>
</div>

<div class="fieldcontain ${hasErrors(bean: barInstance, field: 'lng', 'error')} required">
	<label for="lng">
		<g:message code="bar.lng.label" default="Lng" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="lng" value="${fieldValue(bean: barInstance, field: 'lng')}" required=""/>
</div>

<div class="fieldcontain ${hasErrors(bean: barInstance, field: 'name', 'error')} ">
	<label for="name">
		<g:message code="bar.name.label" default="Name" />
		
	</label>
	<g:textField name="name" value="${barInstance?.name}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: barInstance, field: 'origine', 'error')} ">
	<label for="origine">
		<g:message code="bar.origine.label" default="Origine" />
		
	</label>
	<g:textField name="origine" value="${barInstance?.origine}"/>
</div>

